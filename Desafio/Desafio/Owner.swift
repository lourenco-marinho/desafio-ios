//
//  Owner.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Foundation

struct Owner {
    let author: String
    let avartarURL: String
}