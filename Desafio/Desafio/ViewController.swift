//
//  ViewController.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit
import AlamofireImage

class ViewController: UITableViewController {

    var page = 1
    var selectedRepo: Repository?
    var repositories: [Repository] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.estimatedRowHeight = 120
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.loadRepositories()
    }

    //MARK: Table View Data Source Methods
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == repositories.count - 1 {
            self.loadRepositories()
            return tableView.dequeueReusableCellWithIdentifier("Loading", forIndexPath: indexPath)
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! RepositoryTableViewCell
            
            let repo = repositories[indexPath.row]
            let owner = repositories[indexPath.row].owner
            let url = NSURL(string: owner.avartarURL)!
            
            cell.name.text = repo.name
            cell.descriptionLabel.text = repo.description
            cell.stars.text = "\(repo.stars)"
            cell.forks.text = "\(repo.forks)"
            cell.avatar.af_setImageWithURL(url)
            cell.username.text = owner.author
            
            return cell
        }
    }

    //MARK: Table View Delegate Methods
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        selectedRepo = repositories[indexPath.row]
        performSegueWithIdentifier("PullRequestsViewController", sender: self)
    }
    
    //MARK: Storyboard Methods
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PullRequestsViewController" {
            let controller = segue.destinationViewController as! PullRequestsViewController
            controller.repository = selectedRepo!
        }
    }
    
    //MARK: Custom Methods
    private func loadRepositories() {
        Github.findRepositories(page, completion: { [unowned self] in
            self.repositories.appendContentsOf($0)
            self.page = self.page + 1
            self.tableView.reloadData()
        }, failure: {
            print($0)
        })
    }
}

