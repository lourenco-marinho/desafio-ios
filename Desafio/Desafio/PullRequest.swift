//
//  PullRequest.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Foundation

struct PullRequest {
    let title: String
    let description: String
    let state: String
    let url: String
    let date: String
    let owner: Owner
}
