//
//  Github.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class Github {
    static func findRepositories(page: Int, completion: [Repository] -> Void, failure: NSError -> Void) {
        let url = NSURL(string: "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(page)")!
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        Alamofire.request(.GET, url).validate().responseJSON {response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    let repositories = json["items"].map { (_, items) -> Repository in
                        let name = items["name"].stringValue
                        let description = items["description"].stringValue
                        let stars = items["stargazers_count"].intValue
                        let forks = items["forks"].intValue
                        
                        let author = items["owner"]["login"].stringValue
                        let avartarURL = items["owner"]["avatar_url"].stringValue
                        let owner = Owner(author: author, avartarURL: avartarURL)
                        
                        return Repository(owner: owner, name: name, description: description, stars: stars, forks: forks)
                    }
                    
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    completion(repositories)
                }
            case .Failure(let error):
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                failure(error)
            }
        }
    }
    
    static func pullRequest(repository: Repository, completion: [PullRequest] -> Void) {
        let url = NSURL(string: "https://api.github.com/repos/\(repository.owner.author)/\(repository.name)/pulls?state=all")!
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        Alamofire.request(.GET, url).validate().responseJSON {response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    let pullRequests = json.map { (_, item) -> PullRequest in
                        let title = item["title"].stringValue
                        let description = item["body"].stringValue
                        let state = item["state"].stringValue
                        let url = item["html_url"].stringValue
                        
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
                        let date = dateFormatter.dateFromString(item["created_at"].stringValue)
                        
                        dateFormatter.dateFormat = "dd/MM/yyyy"
                        let stringDate = dateFormatter.stringFromDate(date!)

                        let author = item["user"]["login"].stringValue
                        let avartarURL = item["user"]["avatar_url"].stringValue
                        let owner = Owner(author: author, avartarURL: avartarURL)
                        
                        return PullRequest(title: title, description: description, state: state, url: url, date: stringDate, owner: owner)
                    }
                    
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    completion(pullRequests)
                }
            case .Failure(let error):
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                print(error)
            }
        }
    }
}