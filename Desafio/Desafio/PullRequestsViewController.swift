//
//  PullRequestsViewController.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

class PullRequestsViewController: UITableViewController {
    
    var repository: Repository!
    var pullRequests: [PullRequest] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = repository.name
        
        Github.pullRequest(repository, completion: { [unowned self] in
            self.pullRequests = $0
        })
    }
    
    //MARK: Table View Data Source Methods
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView .dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! PullRequestTableViewCell
        let request = pullRequests[indexPath.row]
        let owner = pullRequests[indexPath.row].owner
        
        cell.title.text = request.title
        cell.descriptionLabel.text = request.description
        cell.date.text = request.date
        
        let url = NSURL(string: owner.avartarURL)!
        cell.avatar.af_setImageWithURL(url)
        cell.username.text = owner.author
        
        return cell
    }
    
    //MARK: Table View Delegate Methods
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let summaryView = NSBundle.mainBundle().loadNibNamed("SummaryView", owner: nil, options: nil)[0] as! SummaryView
        summaryView.configure(pullRequests)
        
        return summaryView
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let pullRequest = pullRequests[indexPath.row]
        
        if let url = NSURL(string: pullRequest.url) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
}