//
//  Repository.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import Foundation

struct Repository {
    let owner: Owner
    let name: String
    let description: String
    let stars: Int
    let forks: Int
}