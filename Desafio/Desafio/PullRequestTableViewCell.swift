//
//  PullRequestTableViewCell.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var date: UILabel!
}