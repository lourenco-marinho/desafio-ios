//
//  RepositoryTableViewCell.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var stars: UILabel!
    @IBOutlet weak var forks: UILabel!
}
