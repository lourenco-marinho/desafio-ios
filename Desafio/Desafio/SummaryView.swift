//
//  IssuesView.swift
//  Desafio
//
//  Created by Lourenço Marinho on 28/04/16.
//  Copyright © 2016 Concrete Solutions. All rights reserved.
//

import UIKit

class SummaryView: UIView {
    @IBOutlet var summaryLabel: UILabel!
    
    func configure(pullRequests: [PullRequest]) {
        let openCount = pullRequests.filter { $0.state == "open" }.count
        let closedCount = pullRequests.count - openCount
        
        let openColor = UIColor(red: 223.0/255.0, green: 145.0/255.0, blue:38.0/255.0, alpha: 1.0)
        let closedColor = UIColor.blackColor()
        let boldFont = UIFont.boldSystemFontOfSize(17)
        
        let openAttributes = [NSForegroundColorAttributeName : openColor, NSFontAttributeName : boldFont]
        let closedAttributes = [NSForegroundColorAttributeName: closedColor, NSFontAttributeName : boldFont]
        let openText = NSMutableAttributedString(string: "\(openCount) opened", attributes: openAttributes)
        let closedText = NSMutableAttributedString(string: " / \(closedCount) closed", attributes: closedAttributes)
        
        let summary = NSMutableAttributedString()
        summary.appendAttributedString(openText)
        summary.appendAttributedString(closedText)

        self.summaryLabel.attributedText = summary
    }
}